<?php
if(!isset($_SESSION)) session_start();
/*
echo "<pre>";
var_dump($_POST);
echo "</pre>";
*/

if(!empty( $_SESSION['PersonInfo']))  $_SESSION['PersonInfo'] .= "[$]";

if(!isset($_SESSION['PersonInfo']))$_SESSION['PersonInfo'] = "";

$_SESSION['PersonInfo'] .=  $_POST['firstName'] . "$#$" .  $_POST['lastName']. "$#$" .  $_POST['ID']. "$#$" .  $_POST['bloodGroup'] ;

$personWiseInfoArray  = explode("[$]", $_SESSION['PersonInfo'] );



?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</head>
<body>




<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">BITM-PHP-B57</a>
        </div>
        <ul class="nav navbar-nav">
            <li class="active"><a  href="destroy_session.php"> Destroy This Session Data </a></li>
            <li><a href="create.php"> Add Person Information </a></li>

        </ul>
    </div>
</nav>


<table class="table table-striped table-bordered">

    <tr>
        <td colspan="4" class="text-center"><h1> List of Persons Information </h1></td>

    </tr>


    <tr>
        <th> First Name </th>
        <th> Last Name </th>
        <th> ID </th>
        <th> Blood Group </th>

    </tr>

    <?php

    foreach($personWiseInfoArray as $singlePersonInfoSting){

        $singlePersonInfoArray = explode("$#$",$singlePersonInfoSting);

        echo "<tr>";

        echo "<td> $singlePersonInfoArray[0] </td>";
        echo "<td> $singlePersonInfoArray[1] </td>";
        echo "<td> $singlePersonInfoArray[2] </td>";
        echo "<td> $singlePersonInfoArray[3] </td>";


        echo "</tr>";

    }

    ?>


</table>

</body>
</html>
